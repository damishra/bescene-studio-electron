_N_E = (window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([
  [9],
  {
    "2kuQ": function (e, t, l) {
      "use strict";
      l.r(t);
      var a = l("q1tI"),
        s = l.n(a),
        n = l("UsKl"),
        u = l("NsyP"),
        i = l("bbC7"),
        o = l("tpaG"),
        r = l("ogtO"),
        c = l("hAcw"),
        d = l("rSlR"),
        p = l("8W3K"),
        _ = l("6nYq"),
        f = l("0Pq/"),
        m = s.a.createElement;
      t.default = function () {
        return m(
          "div",
          { id: "root" },
          m(
            n.a,
            { page: "Projects", username: "Kailey Bradt" },
            m(
              u.a,
              null,
              m(
                u.c,
                null,
                m(i.i, { href: "./" }, m(c.a, null), "Home"),
                m(i.j, { href: "./projects" }, m(d.a, null), "Projects"),
                m(i.i, { href: "./catalog" }, m(p.a, null), "Catalog"),
                m(i.i, { href: "./reports" }, m(_.a, null), "Reporting")
              ),
              m(
                u.b,
                null,
                m(
                  i.i,
                  { href: "./settings" },
                  m(f.a, null),
                  "Account & Settings"
                )
              )
            ),
            m(
              o.a,
              null,
              m(
                i.a,
                null,
                m(i.e, null, "Welcome to Projects."),
                m(i.g, null, "Work on your projects or create new ones")
              ),
              m(
                "div",
                { className: "create" },
                m(i.f, null, "Create"),
                m(
                  "div",
                  { className: "underline" },
                  m("div", { id: "first" }),
                  m("div", null)
                ),
                m(
                  "div",
                  { className: "createBox" },
                  m(
                    i.f,
                    null,
                    "Paste a YouTube or Vimeo URL link of the video you want to work with"
                  ),
                  m(i.c, null, "Your Work will be private until you publish"),
                  m("input", { type: "text" })
                )
              ),
              m(
                "div",
                { className: "create" },
                m(i.f, null, "Projects"),
                m(
                  "div",
                  { className: "underline" },
                  m("div", { id: "second" }),
                  m("div", null)
                )
              ),
              m(
                o.c,
                null,
                m(
                  "div",
                  { className: "projectsBox" },
                  m(r.a, {
                    src: "./assets/Youtube+thumbnails_8a7f52_6693605.jpg",
                    title: "Sample title",
                    status: !0,
                  }),
                  m(r.a, {
                    src: "./assets/Youtube+thumbnails_8a7f52_6693605.jpg",
                    title: "Sample title",
                  }),
                  m(r.a, {
                    src: "./assets/Youtube+thumbnails_8a7f52_6693605.jpg",
                    title: "Sample title",
                    status: !0,
                  }),
                  m(r.a, {
                    src: "./assets/Youtube+thumbnails_8a7f52_6693605.jpg",
                    title: "Sample title",
                  })
                )
              )
            )
          )
        );
      };
    },
    "ZVd/": function (e, t, l) {
      (window.__NEXT_P = window.__NEXT_P || []).push([
        "./projects",
        function () {
          return l("2kuQ");
        },
      ]);
    },
  },
  [["ZVd/", 0, 1, 2]],
]);

_N_E = (window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([
  [10],
  {
    fD73: function (n, e, r) {
      "use strict";
      var t = r("q1tI"),
        a = r.n(t),
        l = r("LYUY");
      e.a = Object(l.a)(
        a.a.createElement("path", {
          d:
            "M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-2h2v2zm0-4h-2V7h2v6z",
        }),
        "Error"
      );
    },
    "o+qX": function (n, e, r) {
      (window.__NEXT_P = window.__NEXT_P || []).push([
        "./reports",
        function () {
          return r("rbS6");
        },
      ]);
    },
    rbS6: function (n, e, r) {
      "use strict";
      r.r(e);
      var t = r("q1tI"),
        a = r.n(t),
        l = r("UsKl"),
        o = r("NsyP"),
        i = r("bbC7"),
        s = r("tpaG"),
        u = (r("ogtO"), r("hAcw")),
        c = r("rSlR"),
        p = r("8W3K"),
        h = r("6nYq"),
        f = r("0Pq/"),
        d = r("fD73"),
        _ = a.a.createElement;
      e.default = function () {
        return _(
          "div",
          { id: "root" },
          _(
            l.a,
            { page: "Reporting", username: "Kailey Bradt" },
            _(
              o.a,
              null,
              _(
                o.c,
                null,
                _(i.i, { href: "./" }, _(u.a, null), "Home"),
                _(i.i, { href: "./projects" }, _(c.a, null), "Projects"),
                _(i.i, { href: "./catalog" }, _(p.a, null), "Catalog"),
                _(i.j, { href: "./reports" }, _(h.a, null), "Reporting")
              ),
              _(
                o.b,
                null,
                _(
                  i.i,
                  { href: "./settings" },
                  _(f.a, null),
                  "Account & Settings"
                )
              )
            ),
            _(
              s.a,
              null,
              _(
                "div",
                { className: "chart-holder" },
                _(
                  "span",
                  null,
                  _("span", { id: "error" }, _(d.a, { fontSize: "large" })),
                  _("span", null, "Module in progress")
                )
              )
            )
          )
        );
      };
    },
  },
  [["o+qX", 0, 1, 2]],
]);

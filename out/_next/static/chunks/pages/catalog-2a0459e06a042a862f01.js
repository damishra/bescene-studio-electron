_N_E = (window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([
  [7],
  {
    Gc60: function (n, a, e) {
      "use strict";
      e.r(a);
      var t = e("q1tI"),
        l = e.n(t),
        r = e("UsKl"),
        o = e("NsyP"),
        i = e("bbC7"),
        u = e("tpaG"),
        s = (e("ogtO"), e("hAcw")),
        c = e("rSlR"),
        p = e("8W3K"),
        h = e("6nYq"),
        f = e("0Pq/"),
        d = e("fD73"),
        _ = l.a.createElement;
      a.default = function () {
        return _(
          "div",
          { id: "root" },
          _(
            r.a,
            { page: "Catalog", username: "Kailey Bradt" },
            _(
              o.a,
              null,
              _(
                o.c,
                null,
                _(i.i, { href: "./" }, _(s.a, null), "Home"),
                _(i.i, { href: "./projects" }, _(c.a, null), "Projects"),
                _(i.j, { href: "./catalog" }, _(p.a, null), "Catalog"),
                _(i.i, { href: "./reports" }, _(h.a, null), "Reporting")
              ),
              _(
                o.b,
                null,
                _(
                  i.i,
                  { href: "./settings" },
                  _(f.a, null),
                  "Account & Settings"
                )
              )
            ),
            _(
              u.a,
              null,
              _(
                "div",
                { className: "chart-holder" },
                _(
                  "span",
                  null,
                  _("span", { id: "error" }, _(d.a, { fontSize: "large" })),
                  _("span", null, "Module in progress")
                )
              )
            )
          )
        );
      };
    },
    I0ip: function (n, a, e) {
      (window.__NEXT_P = window.__NEXT_P || []).push([
        "./catalog",
        function () {
          return e("Gc60");
        },
      ]);
    },
    fD73: function (n, a, e) {
      "use strict";
      var t = e("q1tI"),
        l = e.n(t),
        r = e("LYUY");
      a.a = Object(r.a)(
        l.a.createElement("path", {
          d:
            "M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-2h2v2zm0-4h-2V7h2v6z",
        }),
        "Error"
      );
    },
  },
  [["I0ip", 0, 1, 2]],
]);

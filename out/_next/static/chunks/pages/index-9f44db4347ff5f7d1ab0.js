_N_E = (window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([
  [8],
  {
    H0SL: function (e, n, l) {
      (window.__NEXT_P = window.__NEXT_P || []).push([
        "./",
        function () {
          return l("cMU6");
        },
      ]);
    },
    cMU6: function (e, n, l) {
      "use strict";
      l.r(n);
      var t = l("q1tI"),
        a = l.n(t),
        i = l("UsKl"),
        s = l("NsyP"),
        c = l("bbC7"),
        o = l("tpaG"),
        r = l("ogtO"),
        u = l("hAcw"),
        f = l("rSlR"),
        d = l("8W3K"),
        m = l("6nYq"),
        h = l("0Pq/"),
        p = l("LYUY"),
        v = Object(p.a)(
          a.a.createElement("path", {
            d:
              "M7 18c-1.1 0-1.99.9-1.99 2S5.9 22 7 22s2-.9 2-2-.9-2-2-2zM1 3c0 .55.45 1 1 1h1l3.6 7.59-1.35 2.44C4.52 15.37 5.48 17 7 17h11c.55 0 1-.45 1-1s-.45-1-1-1H7l1.1-2h7.45c.75 0 1.41-.41 1.75-1.03l3.58-6.49c.37-.66-.11-1.48-.87-1.48H5.21l-.67-1.43c-.16-.35-.52-.57-.9-.57H2c-.55 0-1 .45-1 1zm16 15c-1.1 0-1.99.9-1.99 2s.89 2 1.99 2 2-.9 2-2-.9-2-2-2z",
          }),
          "ShoppingCartRounded"
        ),
        b = Object(p.a)(
          a.a.createElement("path", {
            d:
              "M11.8 10.9c-2.27-.59-3-1.2-3-2.15 0-1.09 1.01-1.85 2.7-1.85 1.42 0 2.13.54 2.39 1.4.12.4.45.7.87.7h.3c.66 0 1.13-.65.9-1.27-.42-1.18-1.4-2.16-2.96-2.54V4.5c0-.83-.67-1.5-1.5-1.5S10 3.67 10 4.5v.66c-1.94.42-3.5 1.68-3.5 3.61 0 2.31 1.91 3.46 4.7 4.13 2.5.6 3 1.48 3 2.41 0 .69-.49 1.79-2.7 1.79-1.65 0-2.5-.59-2.83-1.43-.15-.39-.49-.67-.9-.67h-.28c-.67 0-1.14.68-.89 1.3.57 1.39 1.9 2.21 3.4 2.53v.67c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5v-.65c1.95-.37 3.5-1.5 3.5-3.55 0-2.84-2.43-3.81-4.7-4.4z",
          }),
          "AttachMoneyRounded"
        ),
        _ = Object(p.a)(
          a.a.createElement("path", {
            d:
              "M13 1.07V9h7c0-4.08-3.05-7.44-7-7.93zM4 15c0 4.42 3.58 8 8 8s8-3.58 8-8v-4H4v4zm7-13.93C7.05 1.56 4 4.92 4 9h7V1.07z",
          }),
          "MouseRounded"
        ),
        g = l("fD73"),
        N = a.a.createElement;
      n.default = function () {
        return N(
          "div",
          { id: "root" },
          N(
            i.a,
            { page: "Home", username: "Kailey Bradt" },
            N(
              s.a,
              null,
              N(
                s.c,
                null,
                N(c.j, { href: "./" }, N(u.a, null), "Home"),
                N(c.i, { href: "./projects" }, N(f.a, null), "Projects"),
                N(c.i, { href: "./catalog" }, N(d.a, null), "Catalog"),
                N(c.i, { href: "./reports" }, N(m.a, null), "Reporting")
              ),
              N(
                s.b,
                null,
                N(
                  c.i,
                  { href: "./settings" },
                  N(h.a, null),
                  "Account & Settings"
                )
              )
            ),
            N(
              o.a,
              null,
              N(c.a, null, N(c.e, null, "Welcome Home, Kailey")),
              N(
                o.b,
                null,
                N(
                  "div",
                  { style: { display: "inline-flex", marginBottom: "20px" } },
                  N(c.f, null, "Performance since your last login"),
                  N(
                    c.h,
                    { href: "./reports" },
                    "See all performance reporting \xa0>"
                  )
                ),
                N(
                  "div",
                  { className: "info-box" },
                  N(
                    "div",
                    { className: "info" },
                    N(
                      "div",
                      { className: "inner-info-text" },
                      N(c.e, null, "377"),
                      N("br", null),
                      N(c.f, null, "Orders")
                    ),
                    N(
                      "div",
                      { className: "inner-info-icon", id: "shop" },
                      N(v, { fontSize: "inherit" })
                    )
                  ),
                  N(
                    "div",
                    { className: "info" },
                    N(
                      "div",
                      { className: "inner-info-text" },
                      N(c.e, null, "$3,140"),
                      N("br", null),
                      N(c.f, null, "Revenue")
                    ),
                    N(
                      "div",
                      { className: "inner-info-icon", id: "money" },
                      N(b, { fontSize: "inherit" })
                    )
                  ),
                  N(
                    "div",
                    { className: "info" },
                    N(
                      "div",
                      { className: "inner-info-text" },
                      N(c.e, null, "8,866"),
                      N("br", null),
                      N(c.f, null, "Clicks")
                    ),
                    N(
                      "div",
                      { className: "inner-info-icon", id: "clicks" },
                      N(_, { fontSize: "inherit" })
                    )
                  )
                ),
                N(
                  "div",
                  { className: "chart-holder" },
                  N(
                    "span",
                    null,
                    N("span", { id: "error" }, N(g.a, { fontSize: "large" })),
                    N("span", null, "Module in progress")
                  )
                ),
                N(
                  "div",
                  { style: { display: "inline-flex", marginBottom: "20px" } },
                  N(c.f, null, "Recent Projects"),
                  N(c.h, { href: "./projects" }, "See all Projects \xa0>")
                ),
                N(
                  "div",
                  { className: "projectsBox" },
                  N(r.a, {
                    src: "./assets/Youtube+thumbnails_8a7f52_6693605.jpg",
                    title: "Sample title",
                    status: !0,
                  }),
                  N(r.a, {
                    src: "./assets/Youtube+thumbnails_8a7f52_6693605.jpg",
                    title: "Sample title",
                  }),
                  N(r.a, {
                    src: "./assets/Youtube+thumbnails_8a7f52_6693605.jpg",
                    title: "Sample title",
                    status: !0,
                  }),
                  N(r.a, {
                    src: "./assets/Youtube+thumbnails_8a7f52_6693605.jpg",
                    title: "Sample title",
                  })
                )
              )
            )
          )
        );
      };
    },
    fD73: function (e, n, l) {
      "use strict";
      var t = l("q1tI"),
        a = l.n(t),
        i = l("LYUY");
      n.a = Object(i.a)(
        a.a.createElement("path", {
          d:
            "M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-2h2v2zm0-4h-2V7h2v6z",
        }),
        "Error"
      );
    },
  },
  [["H0SL", 0, 1, 2]],
]);

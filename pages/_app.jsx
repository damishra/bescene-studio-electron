import "../styles/index.scss";

export default function CustomApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

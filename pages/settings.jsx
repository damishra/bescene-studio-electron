import Common from "../components/common";
import { Side, SideBottom, SideTop } from "../components/extensions/side";
import {
  NavButton,
  SelectedNavButton,
  HeadOne,
  HeadThree,
  Banner,
  Link,
} from "../components/styled";
import { Canvas, CanvasHome } from "../components/extensions/canvas";
import Project from "../components/extensions/project";

import {
  Home,
  Subscriptions,
  LibraryBooks,
  BarChart,
  Settings,
  ShoppingCartRounded,
  AttachMoneyRounded,
  MouseRounded,
  Error,
} from "@material-ui/icons";
import Axios from "axios";
import { useEffect } from "react";
import { useState } from "react";

const App = () => {
  let [dataConst, addData] = useState([]);
  let [projectCount, incrementCount] = useState(0);

  const handleAddData = (obj) => addData(obj);
  useEffect(() => {
    Axios({
      method: "get",
      url: "http://34.74.175.85/api/1/studio/projects",
      responseType: "json",
    })
      .then(({ data }) => {
        handleAddData(data);
        incrementCount(data.length);
      })
      .catch((err) => console.error(err));
  });

  return (
    <div id="root">
      <Common page="Account & Settings" username={"Kailey Bradt"}>
        <Side>
          <SideTop>
            <NavButton href={"/"}>
              <Home />
              Home
            </NavButton>
            <NavButton href={"/projects"}>
              <Subscriptions />
              Projects
              <span
                style={{
                  float: "right",
                  marginRight: "30px",
                }}
              >
                ({projectCount})
              </span>
            </NavButton>
            <NavButton href={"/catalog"}>
              <LibraryBooks />
              Catalog
            </NavButton>
            <NavButton href={"/reports"}>
              <BarChart />
              Reporting
            </NavButton>
          </SideTop>
          <SideBottom>
            <SelectedNavButton href={"/settings"}>
              <Settings />
              Account & Settings
            </SelectedNavButton>
          </SideBottom>
        </Side>
        <Canvas></Canvas>
      </Common>
    </div>
  );
};

export default App;

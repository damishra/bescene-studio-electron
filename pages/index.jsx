import Common from "../components/common";
import { Side, SideBottom, SideTop } from "../components/extensions/side";
import {
  NavButton,
  SelectedNavButton,
  HeadOne,
  HeadThree,
  Banner,
  PageLink,
} from "../components/styled";
import { Canvas, CanvasHome } from "../components/extensions/canvas";
import Project from "../components/extensions/project";

import {
  Home,
  Subscriptions,
  LibraryBooks,
  BarChart,
  Settings,
  ShoppingCartRounded,
  AttachMoneyRounded,
  MouseRounded,
  Error,
} from "@material-ui/icons";
import Axios from "axios";
import { useEffect } from "react";
import { useState } from "react";

const App = () => {
  let [dataConst, addData] = useState([]);
  let [projectCount, incrementCount] = useState(0);

  const handleAddData = (obj) => addData(obj);
  useEffect(() => {
    Axios({
      method: "get",
      url: "http://34.74.175.85/api/1/studio/projects",
      responseType: "json",
    })
      .then(({ data }) => {
        handleAddData(data);
        incrementCount(data.length);
      })
      .catch((err) => console.error(err));
  });

  return (
    <div id="root">
      <Common page="Home" username={"Kailey Bradt"}>
        <Side>
          <SideTop>
            <SelectedNavButton href={"/"}>
              <Home />
              Home
            </SelectedNavButton>
            <NavButton href={"/projects"}>
              <Subscriptions />
              Projects
              <span
                style={{
                  float: "right",
                  marginRight: "30px",
                }}
              >
                ({projectCount})
              </span>
            </NavButton>
            <NavButton href={"/catalog"}>
              <LibraryBooks />
              Catalog
            </NavButton>
            <NavButton href={"/reports"}>
              <BarChart />
              Reporting
            </NavButton>
          </SideTop>
          <SideBottom>
            <NavButton href={"/settings"}>
              <Settings />
              Account & Settings
            </NavButton>
          </SideBottom>
        </Side>
        <Canvas>
          <Banner>
            <HeadOne>Welcome Home, Kailey</HeadOne>
          </Banner>
          <CanvasHome>
            <div style={{ display: `inline-flex`, marginBottom: `20px` }}>
              <HeadThree>Performance since your last login</HeadThree>
              <PageLink href="/reports">
                See all performance reporting &nbsp;&gt;
              </PageLink>
            </div>
            <div className="info-box">
              <div className="info">
                <div className="inner-info-text">
                  <HeadOne>377</HeadOne>
                  <br />
                  <HeadThree>Orders</HeadThree>
                </div>
                <div className="inner-info-icon" id="shop">
                  <ShoppingCartRounded fontSize="inherit" />
                </div>
              </div>
              <div className="info">
                <div className="inner-info-text">
                  <HeadOne>$3,140</HeadOne>
                  <br />
                  <HeadThree>Revenue</HeadThree>
                </div>
                <div className="inner-info-icon" id="money">
                  <AttachMoneyRounded fontSize="inherit" />
                </div>
              </div>
              <div className="info">
                <div className="inner-info-text">
                  <HeadOne>8,866</HeadOne>
                  <br />
                  <HeadThree>Clicks</HeadThree>
                </div>
                <div className="inner-info-icon" id="clicks">
                  <MouseRounded fontSize="inherit" />
                </div>
              </div>
            </div>
            <div className="chart-holder">
              <span>
                <span id="error">
                  <Error fontSize="large" />
                </span>
                <span>Module in progress</span>
              </span>
            </div>
            <div style={{ display: `inline-flex`, marginBottom: `20px` }}>
              <HeadThree>Recent Projects</HeadThree>
              <PageLink href="/projects">See all Projects &nbsp;&gt;</PageLink>
            </div>
            <div className="projectsBox">
              {dataConst.map(({ title, videoImgUrl, createdTime, id }) => (
                <Project key={id} title={title} src={videoImgUrl} />
              ))}
            </div>
          </CanvasHome>
        </Canvas>
      </Common>
    </div>
  );
};

export default App;

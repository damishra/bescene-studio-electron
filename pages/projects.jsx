import Common from "../components/common";
import { Side, SideBottom, SideTop } from "../components/extensions/side";
import {
  NavButton,
  SelectedNavButton,
  Banner,
  HeadOne,
  HeadTwo,
  HeadThree,
  Link,
  Details,
  HeadFour,
} from "../components/styled";
import { Canvas, CanvasProjects } from "../components/extensions/canvas";
import Project from "../components/extensions/project";

import {
  Home,
  Subscriptions,
  LibraryBooks,
  BarChart,
  Settings,
  ShoppingCartRounded,
  AttachMoneyRounded,
  MouseRounded,
  Error,
} from "@material-ui/icons";
import { useState, useEffect } from "react";
import Axios from "axios";

const App = () => {
  let [dataConst, addData] = useState([]);
  let [projectCount, incrementCount] = useState(0);

  const handleAddData = (obj) => addData(obj);
  useEffect(() => {
    Axios({
      method: "get",
      url: "http://34.74.175.85/api/1/studio/projects",
      responseType: "json",
    })
      .then(({ data }) => {
        handleAddData(data);
        incrementCount(data.length);
      })
      .catch((err) => console.error(err));
  });

  return (
    <div id="root">
      <Common page="Projects" username={"Kailey Bradt"}>
        <Side>
          <SideTop>
            <NavButton href={"/"}>
              <Home />
              Home
            </NavButton>
            <SelectedNavButton href={"/projects"}>
              <Subscriptions />
              Projects
              <span
                style={{
                  float: "right",
                  marginRight: "30px",
                }}
              >
                ({projectCount})
              </span>
            </SelectedNavButton>
            <NavButton href={"/catalog"}>
              <LibraryBooks />
              Catalog
            </NavButton>
            <NavButton href={"/reports"}>
              <BarChart />
              Reporting
            </NavButton>
          </SideTop>
          <SideBottom>
            <NavButton href={"/settings"}>
              <Settings />
              Account & Settings
            </NavButton>
          </SideBottom>
        </Side>
        <Canvas>
          <Banner>
            <HeadOne>Welcome to Projects.</HeadOne>
            <HeadTwo>Work on your projects or create new ones</HeadTwo>
          </Banner>
          <div className="create">
            <HeadThree>Create</HeadThree>
            <div className="underline">
              <div id="first"></div>
              <div></div>
            </div>
            <div className="createBox">
              <HeadThree>
                Paste a YouTube or Vimeo URL link of the video you want to work
                with
              </HeadThree>
              <Details>Your Work will be private until you publish</Details>
              <input type="text"></input>
            </div>
          </div>

          <div className="create">
            <HeadThree>Projects</HeadThree>
            <div className="underline">
              <div id="second"></div>
              <div></div>
            </div>
          </div>
          <CanvasProjects>
            <div className="projectsBox">
              {dataConst.map(({ title, videoImgUrl, createdTime, id }) => (
                <Project key={id} title={title} src={videoImgUrl} />
              ))}
            </div>
          </CanvasProjects>
        </Canvas>
      </Common>
    </div>
  );
};

export default App;

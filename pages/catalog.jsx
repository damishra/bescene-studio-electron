import Common from "../components/common";
import { Side, SideBottom, SideTop } from "../components/extensions/side";
import {
  NavButton,
  SelectedNavButton,
  HeadOne,
  HeadThree,
  Banner,
  Link,
} from "../components/styled";
import { Canvas, CanvasHome } from "../components/extensions/canvas";
import Project from "../components/extensions/project";

import {
  Home,
  Subscriptions,
  LibraryBooks,
  BarChart,
  Settings,
  ShoppingCartRounded,
  AttachMoneyRounded,
  MouseRounded,
  Error,
} from "@material-ui/icons";
import Axios from "axios";
import { useEffect } from "react";
import { useState } from "react";

const App = () => {
  let [dataConst, addData] = useState([]);
  let [projectCount, incrementCount] = useState(0);
  let [activeProducts, setActiveProducts] = useState([]);

  const handleAddData = (obj) => addData(obj);
  useEffect(() => {
    Axios({
      method: "get",
      url: "http://34.74.175.85/api/1/studio/projects",
      responseType: "json",
    })
      .then(({ data }) => {
        handleAddData(data);
        incrementCount(data.length);
      })
      .catch((err) => console.error(err));
  });

  return (
    <div id="root">
      <Common page="Catalog" username={"Kailey Bradt"}>
        <Side>
          <SideTop>
            <NavButton href={"/"}>
              <Home />
              Home
            </NavButton>
            <NavButton href={"/projects"}>
              <Subscriptions />
              Projects
              <span
                style={{
                  float: "right",
                  marginRight: "30px",
                }}
              >
                ({projectCount})
              </span>
            </NavButton>
            <SelectedNavButton href={"/catalog"}>
              <LibraryBooks />
              Catalog
            </SelectedNavButton>
            <NavButton href={"/reports"}>
              <BarChart />
              Reporting
            </NavButton>
          </SideTop>
          <SideBottom>
            <NavButton href={"/settings"}>
              <Settings />
              Account & Settings
            </NavButton>
          </SideBottom>
        </Side>
        <Canvas>
          <div className="chart-holder">
            <span>
              <span id="error">
                <Error fontSize="large" />
              </span>
              <span>Module in progress</span>
            </span>
          </div>
        </Canvas>
      </Common>
    </div>
  );
};

export default App;

import Top from "../components/extensions/top";

const Common = (props) => {
  return (
    <div>
      <Top page={props.page} username={props.username} />
      <main>{props.children}</main>
    </div>
  );
};

export default Common;

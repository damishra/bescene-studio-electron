import styles from '../../styles/core/projects.module.scss';

const VideoCard = props => (
  <div className={styles.video_card}>
    <img
      className={styles.thumbnail}
      src={props.src}
      alt={props.alt}
      id={props.id}
    />
    <div className={styles.info}>
      <div className={styles.title}>{props.title}</div>
      <div className={styles.timestamp}>{props.timestamp}</div>
    </div>
  </div>
);

const Projects = () => {
  return (
    <div className={styles.projectRoot}>
      <section className={styles.top}>
        <span>projects</span>
        <span>create</span>
        <span>draft</span>
      </section>
      <section className={styles.projectList}></section>
    </div>
  );
};

export default Projects;

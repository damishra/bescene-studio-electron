import styles from "../styles/styled.module.scss";
import Link from "next/link";

const Button = (props) => (
  <button className={styles.button}>{props.children}</button>
);

const MenuButton = (props) => (
  <button className={styles.menuButton}>{props.children}</button>
);

const ProjectButton = (props) => (
  <button className={styles.projectButton}>{props.children}</button>
);

const NavButton = (props) => (
  <div className={styles.navButton}>
    <Link href={props.href}>
      <a>{props.children}</a>
    </Link>
  </div>
);

const SelectedNavButton = (props) => (
  <div className={styles.selected}>{props.children}</div>
);

const HeadOne = (props) => (
  <div className={styles.headingOne}>{props.children}</div>
);

const HeadTwo = (props) => (
  <div className={styles.headingTwo}>{props.children}</div>
);

const HeadThree = (props) => (
  <div className={styles.headingThree}>{props.children}</div>
);

const HeadFour = (props) => (
  <div className={styles.headingFour}>{props.children}</div>
);

const Details = (props) => (
  <div className={styles.detailText}>{props.children}</div>
);

const Banner = (props) => <div className={styles.banner}>{props.children}</div>;

const PageLink = (props) => (
  <div>
    <Link href={props.href}>
      <a className={styles.link}>{props.children}</a>
    </Link>
  </div>
);

export {
  Button,
  MenuButton,
  ProjectButton,
  NavButton,
  SelectedNavButton,
  HeadOne,
  HeadTwo,
  HeadThree,
  HeadFour,
  Banner,
  Details,
  PageLink,
};

import styles from "../../styles/extensions/project.module.scss";
import { MoreHoriz } from "@material-ui/icons";
import { HeadFour, Details } from "../styled";

const now = new Date();
const calcTimePassed = (epoch) => {
  let secondsPassed = Math.round((now.getTime() - epoch) / 1000);
  if (secondsPassed < 60) return `Updated just now`;
  else if (secondsPassed == 60) return `Updated 1 minute ago`;
  else if (secondsPassed < 3600)
    return `Updated ${Math.round(secondsPassed / 60)} minutes ago`;
  else if (secondsPassed == 3600) return `Updated 1 hour ago`;
  else if (secondsPassed < 86400)
    return `Updated ${Math.round(secondsPassed / 3600)} hours ago`;
  else if (secondsPassed == 86400) return `Updated 1 day ago`;
  else if (secondsPassed < 604800)
    return `Updated ${Math.round(secondsPassed / 86400)} days ago`;
};

const Project = (props) => {
  return (
    <div className={styles.project}>
      <img className={styles.thumb} src={props.src} />
      <div className={styles.details}>
        <div className={styles.upper}>
          <div className={styles.title}>
            <HeadFour>{props.title.substring(0, 21) + "..."}</HeadFour>
          </div>
          <div className={styles.more}>
            <MoreHoriz fontSize="inherit" style={{ float: "right" }} />
          </div>
        </div>
        <div className={styles.lower}>
          <Details>
            <div className={styles.epoch}>{`Updated 1 day ago`}</div>
          </Details>
          {props.status ? (
            <Details>
              <div className={styles.active} style={{ float: "right" }}>
                Active
              </div>
            </Details>
          ) : (
            <Details>
              <div className={styles.inactive} style={{ float: "right" }}>
                Inactive
              </div>
            </Details>
          )}
        </div>
      </div>
    </div>
  );
};

export default Project;

const TitleBar = () => (
  <header id="titlebar">
    <div id="drag-region">
      <div id="window-title">
        <span>BeScene Studio</span>
      </div>
      <div id="window-controls">
        <div class="button" id="min-button">
          <img
            class="icon"
            srcset="assets/min-w-10.png 1x, assets/min-w-12.png 1.25x, assets/min-w-15.png 1.5x, assets/min-w-15.png 1.75x, assets/min-w-20.png 2x, assets/min-w-20.png 2.25x, assets/min-w-24.png 2.5x, assets/min-w-30.png 3x, assets/min-w-30.png 3.5x"
            draggable="false"
          />
        </div>
        <div class="button" id="max-button">
          <img
            class="icon"
            srcset="assets/max-w-10.png 1x, assets/max-w-12.png 1.25x, assets/max-w-15.png 1.5x, assets/max-w-15.png 1.75x, assets/max-w-20.png 2x, assets/max-w-20.png 2.25x, assets/max-w-24.png 2.5x, assets/max-w-30.png 3x, assets/max-w-30.png 3.5x"
            draggable="false"
          />
        </div>
        <div class="button" id="restore-button">
          <img
            class="icon"
            srcset="assets/restore-w-10.png 1x, assets/restore-w-12.png 1.25x, assets/restore-w-15.png 1.5x, assets/restore-w-15.png 1.75x, assets/restore-w-20.png 2x, assets/restore-w-20.png 2.25x, assets/restore-w-24.png 2.5x, assets/restore-w-30.png 3x, assets/restore-w-30.png 3.5x"
            draggable="false"
          />
        </div>
        <div class="button" id="close-button">
          <img
            class="icon"
            srcset="assets/close-w-10.png 1x, assets/close-w-12.png 1.25x, assets/close-w-15.png 1.5x, assets/close-w-15.png 1.75x, assets/close-w-20.png 2x, assets/close-w-20.png 2.25x, assets/close-w-24.png 2.5x, assets/close-w-30.png 3x, assets/close-w-30.png 3.5x"
            draggable="false"
          />
        </div>
      </div>
    </div>
  </header>
);

export default TitleBar;

import styles from "../../styles/extensions/top.module.scss";
import { Button } from "../styled";

const Top = (props) => {
  return (
    <section className={styles.topNav}>
      <div className={styles.logo}>
        <img src="/assets/WebApp_Icon.png" /> <text>Studio</text>
      </div>
      <div className={styles.mid}>
        <div className={styles.page}>{props.page}</div>
        <div className={styles.create}>
          <Button className={styles.button}>create</Button>
        </div>
      </div>
      <div className={styles.account}>
        <div className={styles.username}>
          <text>{props.username}</text>
          <img src="/assets/Randy+Krum+Profile+Photo+square.jpg"></img>
        </div>
      </div>
    </section>
  );
};

export default Top;

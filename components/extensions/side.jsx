import styles from "../../styles/extensions/side.module.scss";

const SideTop = (props) => {
  return (
    <div className={styles.top}>
      {props.children}
      <div className={styles.divider}></div>
    </div>
  );
};

const SideBottom = (props) => {
  return (
    <div className={styles.bottom}>
      <div className={styles.divider}></div>
      {props.children}
    </div>
  );
};

const Side = (props) => {
  return <div className={styles.sideNav}>{props.children}</div>;
};

export { Side, SideBottom, SideTop };

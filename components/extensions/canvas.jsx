import styles from "../../styles/extensions/canvas.module.scss";

const Canvas = (props) => {
  return <div className={styles.canvas}>{props.children}</div>;
};

const CanvasHome = (props) => {
  return <div className={styles.canvasHome}>{props.children}</div>;
};

const CanvasProjects = (props) => {
  return <div className={styles.canvasProjects}>{props.children}</div>;
};

export { Canvas, CanvasHome, CanvasProjects };
